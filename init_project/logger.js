const log = (type,message) => {
    if(type == "e")
        console.log("Server Error:"+message);
    else if(type == "d")
        console.log("Server Debugging:"+message);
}

module.exports = log;