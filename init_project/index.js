const express = require('express');
const app = express();
const log = require('./logger');
const excel = require('./ExcelReader');
const formidable = require('formidable');
const fs = require('fs');



app.get('/',(req,res) => {
    res.send(excel);
});

app.get('/upload',(req,res)=>{
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write('<form action="upload" method="post" enctype="multipart/form-data">');
    res.write('<input type="file" name="filetoupload"><br>');
    res.write('<input type="submit">');
    res.write('</form>');
    return res.end();
})

app.post('/upload',(req,res) =>{
    if (req.url == '/upload') {
        var form = new formidable.IncomingForm();
        form.parse(req, function (err, fields, files) {
          var oldpath = files.filetoupload.path;
          var newpath = './import'

          fs.copyFile(oldpath, newpath, function (err) {
            if (err) throw err;
            res.write('File uploaded and moved!');
            res.end();
          });
     });
      } else {
        res.writeHead(200, {'Content-Type': 'text/html'});
        res.write('<form action="upload" method="post" enctype="multipart/form-data">');
        res.write('<input type="file" name="filetoupload"><br>');
        res.write('<input type="submit">');
        res.write('</form>');
        return res.end();
      }
});





const port = process.env.PORT || 3000
app.listen(port, () => {
    log("d",`Listen to port: ${port}`)
})